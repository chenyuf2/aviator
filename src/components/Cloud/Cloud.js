import { COLOR_PALLETE } from "../../utils/Colors";
import { useRef } from "react";
import { useFrame } from "@react-three/fiber";
const CloudMesh = ({ position, rotation, scale }) => {
  return (
    <mesh
      position={position}
      rotation={rotation}
      scale={scale}
      castShadow={true}
      receiveShadow={true}
    >
      <boxGeometry args={[20, 20, 20]} />
      <meshPhongMaterial color={COLOR_PALLETE.WHITE} />
    </mesh>
  );
};

const Cloud = ({ position, rotation, scale }) => {
  const numBlocks = 3 + Math.floor(Math.random() * 3);
  const ref = useRef(null);
  useFrame(() => {
    for (let i = 0; i < ref.current.children.length; i++) {
      const meshItem = ref.current.children[i];
      meshItem.rotation.z += Math.random() * 0.005 * (i + 1);
      meshItem.rotation.y += Math.random() * 0.002 * (i + 1);
    }
  });
  return (
    <group ref={ref} position={position} rotation={rotation} scale={scale}>
      {Array.from({ length: numBlocks }).map((_, index) => {
        const position = [index * 15, Math.random() * 10, Math.random() * 10];
        const rotation = [
          0,
          Math.random() * Math.PI * 2,
          Math.random() * Math.PI * 2,
        ];
        const scale = [
          0.1 + Math.random() * 0.9,
          0.1 + Math.random() * 0.9,
          0.1 + Math.random() * 0.9,
        ];

        return (
          <CloudMesh
            key={index}
            scale={scale}
            rotation={rotation}
            position={position}
          />
        );
      })}
    </group>
  );
};

export default Cloud;
