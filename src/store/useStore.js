import create from "zustand";
import { normalizeFn } from "../utils/UtilsFn";
import * as THREE from "three";
import { gsap, Power2 } from "gsap";
import { COLOR_PALLETE } from "../utils/Colors";
const useStore = create((set, get) => ({
  speed: 0,
  initSpeed: 0.00035,
  baseSpeed: 0.00035,
  targetBaseSpeed: 0.00035,
  incrementSpeedByTime: 0.0000025,
  incrementSpeedByLevel: 0.000005,
  distanceForSpeedUpdate: 100,
  speedLastUpdate: 0,

  distance: 0,
  ratioSpeedDistance: 50,
  energy: 100,
  ratioSpeedEnergy: 3,

  level: 1,
  levelLastUpdate: 0,
  distanceForLevelUpdate: 1000,

  planeDefaultHeight: 100,
  planeAmpHeight: 80,
  planeAmpWidth: 75,
  planeMoveSensivity: 0.005,
  planeRotXSensivity: 0.0008,
  planeRotZSensivity: 0.0004,
  planeFallSpeed: 0.001,
  planeMinSpeed: 1.2,
  planeMaxSpeed: 1.6,
  planeSpeed: 0,
  planeCollisionDisplacementX: 0,
  planeCollisionSpeedX: 0,

  planeCollisionDisplacementY: 0,
  planeCollisionSpeedY: 0,

  seaRadius: 600,
  seaLength: 800,
  //seaRotationSpeed:0.006,
  wavesMinAmp: 5,
  wavesMaxAmp: 20,
  wavesMinSpeed: 0.001,
  wavesMaxSpeed: 0.003,

  cameraFarPos: 500,
  cameraNearPos: 150,
  cameraSensivity: 0.002,

  coinDistanceTolerance: 15,
  coinValue: 3,
  coinsSpeed: 0.5,
  coinLastSpawn: 0,
  distanceForCoinsSpawn: 100,

  ennemyDistanceTolerance: 10,
  ennemyValue: 10,
  ennemiesSpeed: 0.6,
  ennemyLastSpawn: 0,
  distanceForEnnemiesSpawn: 50,

  status: "playing",
  strokeDashOffset: 502,
  replayVisible: false,
  coinsInUse: [],
  coinsPool: Array.from({ length: 20 }).map((_) => ({
    angle: 0,
    dist: 0,
    exploding: false,
    rotation: [0, 0, 0],
  })),
  ennemiesInUse: [],
  ennemiesPool: Array.from({ length: 10 }).map((item) => ({
    angle: 0,
    dist: 0,
  })),
  ambientLightIntensity: 0.5,
  spawnEnnemies: () => {
    const nEnnemies = get().level;
    const copiedEnnemiesPool = [...get().ennemiesPool];
    for (let i = 0; i < nEnnemies; i++) {
      let ennemy = { angle: 0, dist: 0 };
      if (copiedEnnemiesPool.length) {
        ennemy = copiedEnnemiesPool.pop();
        set((state) => ({
          ennemiesPool: copiedEnnemiesPool,
        }));
      }

      ennemy.angle = -(i * 0.1);
      ennemy.dist =
        get().seaRadius +
        get().planeDefaultHeight +
        (-1 + Math.random() * 2) * (get().planeAmpHeight - 20);

      set((state) => ({
        ennemiesInUse: [...get().ennemiesInUse, ennemy],
      }));
    }
  },
  rotateEnnemies: (delta, planeRef, particlesHolder) => {
    const copiedEnnemiesPool = [...get().ennemiesPool];
    const copiedEnnemiesInUse = [...get().ennemiesInUse];
    for (let i = 0; i < copiedEnnemiesInUse.length; i++) {
      let ennemy = copiedEnnemiesInUse[i];
      ennemy.angle += get().speed * delta * get().ennemiesSpeed;

      if (ennemy.angle > Math.PI * 2) ennemy.angle -= Math.PI * 2;

      const curEnnemyPosition = [
        Math.cos(ennemy.angle) * ennemy.dist,
        -get().seaRadius + Math.sin(ennemy.angle) * ennemy.dist,
        0,
      ];

      const curEnnemyPositionVec = new THREE.Vector3(
        curEnnemyPosition[0],
        curEnnemyPosition[1],
        curEnnemyPosition[2]
      );
      const diffPos = planeRef.current.position
        .clone()
        .sub(curEnnemyPositionVec);

      var d = diffPos.length();
      if (d < get().ennemyDistanceTolerance) {
        get().spawnParticles(
          curEnnemyPositionVec.clone(),
          15,
          COLOR_PALLETE.RED,
          3,
          particlesHolder
        );

        copiedEnnemiesPool.unshift(copiedEnnemiesInUse.splice(i, 1)[0]);

        set((state) => ({
          planeCollisionSpeedX: (100 * diffPos.x) / d,
          planeCollisionSpeedY: (100 * diffPos.y) / d,
          ambientLightIntensity: 2,
        }));

        get().removeEnergy();
        i--;
      } else if (ennemy.angle > Math.PI) {
        copiedEnnemiesPool.unshift(copiedEnnemiesInUse.splice(i, 1)[0]);
        i--;
      }
    }
    set((state) => ({
      ennemiesInUse: copiedEnnemiesInUse,
      ennemiesPool: copiedEnnemiesPool,
    }));
  },
  generateParticle: () => {
    const geom = new THREE.TetrahedronGeometry(3, 0);
    const mat = new THREE.MeshPhongMaterial({
      color: 0x009999,
      shininess: 0,
      specular: 0xffffff,
      shading: THREE.FlatShading,
    });
    const mesh = new THREE.Mesh(geom, mat);
    return mesh;
  },
  particlesPool: Array.from({ length: 10 }).map((_, index) => {
    const geom = new THREE.TetrahedronGeometry(3, 0);
    const mat = new THREE.MeshPhongMaterial({
      color: 0x009999,
      shininess: 0,
      specular: 0xffffff,
      shading: THREE.FlatShading,
    });
    const mesh = new THREE.Mesh(geom, mat);
    return mesh;
  }),
  particlesInUse: [],
  particleExplode: (pos, color, scale, particleMesh, particlesHolder) => {
    particleMesh.material.color = new THREE.Color(color);
    particleMesh.material.needsUpdate = true;
    particleMesh.scale.set(scale, scale, scale);
    const targetX = pos.x + (-1 + Math.random() * 2) * 50;
    const targetY = pos.y + (-1 + Math.random() * 2) * 50;
    const speed = 0.6 + Math.random() * 0.2;
    gsap.to(particleMesh.rotation, {
      duration: speed,
      x: Math.random() * 12,
      y: Math.random() * 12,
    });
    gsap.to(particleMesh.scale, { duration: speed, x: 0.1, y: 0.1, z: 0.1 });
    gsap.to(particleMesh.position, {
      duration: speed,
      x: targetX,
      y: targetY,
      delay: Math.random() * 0.1,
      ease: Power2.easeOut,
      onComplete: function () {
        if (particlesHolder) particlesHolder.remove(particleMesh);
        particleMesh.scale.set(1, 1, 1);
        set((state) => ({
          particlesPool: [particleMesh, ...state.particlesPool],
        }));
      },
    });
  },
  spawnParticles: (pos, density, color, scale, particlesHolder) => {
    const nPArticles = density;
    const copiedParticlesPool = [...get().particlesPool];

    for (let i = 0; i < nPArticles; i++) {
      let particle;
      if (copiedParticlesPool.length) {
        particle = copiedParticlesPool.pop();
      } else {
        particle = get().generateParticle();
      }

      particlesHolder.add(particle);

      // particle.visible = true;
      particle.position.y = pos.y;
      particle.position.x = pos.x;
      get().particleExplode(pos, color, scale, particle, particlesHolder);
    }
    set((state) => ({
      particlesPool: copiedParticlesPool,
    }));
  },
  rotateCoins: (delta, planeRef, particlesHolder) => {
    const copiedCoinsInUse = [...get().coinsInUse];
    const copiedCoinsPool = [...get().coinsPool];
    for (let i = 0; i < copiedCoinsInUse.length; i++) {
      let coin = copiedCoinsInUse[i];
      if (coin.exploding) continue;
      coin.angle += get().speed * delta * get().coinsSpeed;
      if (coin.angle > Math.PI * 2) coin.angle -= Math.PI * 2;

      const curCoinPosition = [
        Math.cos(coin.angle) * coin.dist,
        -get().seaRadius + Math.sin(coin.angle) * coin.dist,
        0,
      ];
      const curCoinPositionVec = new THREE.Vector3(
        curCoinPosition[0],
        curCoinPosition[1],
        curCoinPosition[2]
      );
      const diffPos = planeRef.current.position.clone().sub(curCoinPositionVec);

      const d = diffPos.length();

      if (d < get().coinDistanceTolerance) {
        copiedCoinsPool.unshift(copiedCoinsInUse.splice(i, 1)[0]);
        get().spawnParticles(
          curCoinPositionVec.clone(),
          5,
          0x009999,
          0.8,
          particlesHolder
        );
        get().addEnergy();
        i--;
      } else if (coin.angle > Math.PI) {
        copiedCoinsPool.unshift(copiedCoinsInUse.splice(i, 1)[0]);
        i--;
      }
    }
    set((state) => ({
      coinsInUse: copiedCoinsInUse,
      coinsPool: copiedCoinsPool,
    }));
  },
  spawnCoins: () => {
    const numCoins = 1 + Math.floor(Math.random() * 10);
    const d =
      get().seaRadius +
      get().planeDefaultHeight +
      (-1 + Math.random() * 2) * (get().planeAmpHeight - 20);
    const amplitude = 10 + Math.round(Math.random() * 10);
    for (let i = 0; i < numCoins; i++) {
      let coin = { angle: 0, dist: 0, exploding: false };
      if (get().coinsPool.length) {
        const copiedCoinsPool = [...get().coinsPool];
        coin = { ...copiedCoinsPool.pop() };
        set((state) => ({
          coinsPool: copiedCoinsPool,
        }));
      }

      const coinDistance = d + Math.cos(i * 0.5) * amplitude;
      const coinAngle = -(i * 0.02);
      coin.angle = coinAngle;
      coin.dist = coinDistance;
      const copiedCoinsInUse = [...get().coinsInUse];
      set((state) => ({
        coinsInUse: [...state.coinsInUse, coin],
      }));
    }
  },

  updateDistance: (delta) => {
    set((state) => ({
      distance: state.distance + state.speed * delta * state.ratioSpeedDistance,
    }));
  },

  addEnergy: () =>
    set((state) => ({
      energy: Math.min(state.energy + state.coinValue, 100),
    })),

  removeEnergy: () =>
    set((state) => ({
      energy: Math.max(state.energy - state.ennemyValue, 0),
    })),

  updateEnergy: (delta) => {
    const energyBar = document.getElementById("energyBar");
    set((state) => ({
      energy: Math.max(
        0,
        state.energy - state.speed * delta * state.ratioSpeedEnergy
      ),
    }));
    energyBar.style.right = 100 - get().energy + "%";
    energyBar.style.backgroundColor = get().energy < 50 ? "#f25346" : "#68c3c0";
    if (get().energy < 30) {
      energyBar.style.animationName = "blinking";
    } else {
      energyBar.style.animationName = "none";
    }

    if (get().energy < 1) {
      set((_) => ({
        status: "gameover",
      }));
    }
  },

  setReplayVisible: () =>
    set((_) => ({
      replayVisible: true,
    })),

  setReplayInvisible: () =>
    set((_) => ({
      replayVisible: false,
    })),

  updatePlane: (delta, mouse, planeRef) => {
    let targetY = normalizeFn(
      mouse.y,
      -0.75,
      0.75,
      get().planeDefaultHeight - get().planeAmpHeight,
      get().planeDefaultHeight + get().planeAmpHeight
    );
    let targetX = normalizeFn(
      mouse.x,
      -1,
      1,
      -get().planeAmpWidth * 0.7,
      -get().planeAmpWidth
    );
    set((state) => ({
      planeSpeed: normalizeFn(
        mouse.x,
        -0.5,
        0.5,
        state.planeMinSpeed,
        state.planeMaxSpeed
      ),
      planeCollisionDisplacementX:
        state.planeCollisionDisplacementX + state.planeCollisionSpeedX,
      planeCollisionDisplacementY:
        state.planeCollisionDisplacementY + state.planeCollisionSpeedY,
    }));
    //maybe has a problem because set is asychronous
    targetX += get().planeCollisionDisplacementX;
    targetY += get().planeCollisionDisplacementY;
    planeRef.current.position.y +=
      (targetY - planeRef.current.position.y) *
      delta *
      get().planeMoveSensivity;
    planeRef.current.position.x +=
      (targetX - planeRef.current.position.x) *
      delta *
      get().planeMoveSensivity;
    planeRef.current.rotation.z =
      (targetY - planeRef.current.position.y) *
      delta *
      get().planeRotXSensivity;
    planeRef.current.rotation.x =
      (planeRef.current.position.y - targetY) *
      delta *
      get().planeRotZSensivity;
    set((state) => ({
      planeCollisionSpeedX:
        state.planeCollisionSpeedX +
        (0 - state.planeCollisionSpeedX) * delta * 0.03,
      planeCollisionDisplacementX:
        state.planeCollisionDisplacementX +
        (0 - state.planeCollisionDisplacementX) * delta * 0.01,
      planeCollisionSpeedY:
        state.planeCollisionSpeedY +
        (0 - state.planeCollisionSpeedY) * delta * 0.03,
      planeCollisionDisplacementY:
        state.planeCollisionDisplacementY +
        (0 - state.planeCollisionDisplacementY) * delta * 0.01,
    }));
  },
  handleReplay: () => {
    console.log(get().status);
    if (get().status === "waitingReplay") {
      set((state) => ({
        speed: 0,
        initSpeed: 0.00035,
        baseSpeed: 0.00035,
        targetBaseSpeed: 0.00035,
        incrementSpeedByTime: 0.0000025,
        incrementSpeedByLevel: 0.000005,
        distanceForSpeedUpdate: 100,
        speedLastUpdate: 0,

        distance: 0,
        ratioSpeedDistance: 50,
        energy: 100,
        ratioSpeedEnergy: 3,

        level: 1,
        levelLastUpdate: 0,
        distanceForLevelUpdate: 1000,

        planeDefaultHeight: 100,
        planeAmpHeight: 80,
        planeAmpWidth: 75,
        planeMoveSensivity: 0.005,
        planeRotXSensivity: 0.0008,
        planeRotZSensivity: 0.0004,
        planeFallSpeed: 0.001,
        planeMinSpeed: 1.2,
        planeMaxSpeed: 1.6,
        planeSpeed: 0,
        planeCollisionDisplacementX: 0,
        planeCollisionSpeedX: 0,

        planeCollisionDisplacementY: 0,
        planeCollisionSpeedY: 0,

        seaRadius: 600,
        seaLength: 800,
        //seaRotationSpeed:0.006,
        wavesMinAmp: 5,
        wavesMaxAmp: 20,
        wavesMinSpeed: 0.001,
        wavesMaxSpeed: 0.003,

        cameraFarPos: 500,
        cameraNearPos: 150,
        cameraSensivity: 0.002,

        coinDistanceTolerance: 15,
        coinValue: 3,
        coinsSpeed: 0.5,
        coinLastSpawn: 0,
        distanceForCoinsSpawn: 100,

        ennemyDistanceTolerance: 10,
        ennemyValue: 10,
        ennemiesSpeed: 0.6,
        ennemyLastSpawn: 0,
        distanceForEnnemiesSpawn: 50,

        status: "playing",
        strokeDashOffset: 502,
        replayVisible: false,
        coinsInUse: [],
        coinsPool: Array.from({ length: 20 }).map((_) => ({
          angle: 0,
          dist: 0,
          exploding: false,
          rotation: [0, 0, 0],
        })),
        ennemiesInUse: [],
        ennemiesPool: Array.from({ length: 10 }).map((item) => ({
          angle: 0,
          dist: 0,
        })),
        ambientLightIntensity: 0.5,
      }));
      get().setReplayInvisible();
    }
  },
}));

export default useStore;
