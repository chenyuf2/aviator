import Coin from "./Coin/Coin";
import { useState } from "react";
import useStore from "../../store/useStore";
const CoinsHolder = () => {
  //possible two dynamic fields, coinsInUse, coinsPool
  const coinsInUse = useStore((state) => state.coinsInUse);
  return (
    <>
      {coinsInUse?.map((item, index) => (
        <Coin
          key={index}
          angle={item.angle}
          dist={item.dist}
          exploding={item.exploding}
          rotation={item.rotation}
        />
      ))}
    </>
  );
};

export default CoinsHolder;
