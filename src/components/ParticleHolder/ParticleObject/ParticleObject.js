const ParticleObject = () => {
  return (
    <mesh>
      <tetrahedronGeometry args={[3, 0]} />
      <meshPhongMaterial
        color={0x009999}
        shininess={0}
        specular={0xffffff}
        flatShading
      />
    </mesh>
  );
};

export default ParticleObject;
