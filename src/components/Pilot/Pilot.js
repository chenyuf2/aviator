import { COLOR_PALLETE } from "../../utils/Colors";
import { useMemo, useState } from "react";
import * as THREE from "three";
import { useFrame } from "@react-three/fiber";
import React, { useRef } from "react";
import useStore from "../../store/useStore";
import { expandDeltaTime } from "../../utils/UtilsFn";
const Pilot = () => {
  const hairSideGeometry = new THREE.BoxGeometry(12, 4, 2);
  hairSideGeometry.applyMatrix4(new THREE.Matrix4().makeTranslation(-6, 0, 0));

  const hairGeometry = new THREE.BoxGeometry(4, 4, 4);
  hairGeometry.applyMatrix4(new THREE.Matrix4().makeTranslation(0, 2, 0));

  const [angleHairs, setAngleHairs] = useState(0);
  const speed = useStore((state) => state.speed);
  useFrame((_, delta) => {
    delta = expandDeltaTime(delta);
    const hairsArray = hairTopRef.current.children;
    for (let i = 0; i < hairsArray.length; i++) {
      const hair = hairsArray[i];
      hair.scale.y = 0.75 + Math.cos(angleHairs + i / 3) * 0.25;
    }
    setAngleHairs(angleHairs + speed * delta * 10);
  });

  const hairTopRef = useRef(null);

  return (
    <mesh position={[-10, 27, 0]}>
      {/* body */}
      <mesh position={[2, -12, 0]}>
        <boxGeometry args={[15, 15, 15]} />
        <meshPhongMaterial color={COLOR_PALLETE.BROWN} flatShading />
      </mesh>
      {/* face */}
      <mesh>
        <boxGeometry args={[10, 10, 10]} />
        <meshLambertMaterial color={COLOR_PALLETE.PINK} />
      </mesh>

      {/* hairs */}

      <mesh position={[-5, 5, 0]}>
        {/* hair Top*/}
        <mesh ref={hairTopRef}>
          {Array.from({ length: 12 }).map((_, index) => {
            const col = index % 3;
            const row = Math.floor(index / 3);
            const startPosZ = -4;
            const startPosX = -4;

            return (
              <mesh
                key={index}
                geometry={hairGeometry}
                position={[startPosX + row * 4, 0, startPosZ + col * 4]}
              >
                <meshLambertMaterial color={COLOR_PALLETE.BROWN} />
              </mesh>
            );
          })}
        </mesh>

        {/* hair side left*/}
        <mesh position={[8, -2, 6]} geometry={hairSideGeometry}>
          <meshLambertMaterial color={COLOR_PALLETE.BROWN} />
        </mesh>
        {/* hair side right*/}
        <mesh position={[8, -2, -6]} geometry={hairSideGeometry}>
          <meshLambertMaterial color={COLOR_PALLETE.BROWN} />
        </mesh>
        {/* hair back side */}
        <mesh position={[-1, -4, 0]}>
          <boxGeometry args={[2, 8, 10]} />
          <meshLambertMaterial color={COLOR_PALLETE.BROWN} />
        </mesh>
      </mesh>

      {/* glass left*/}
      <mesh position={[6, 0, 3]}>
        <boxGeometry args={[5, 5, 5]} />
        <meshLambertMaterial color={COLOR_PALLETE.BROWN} />
      </mesh>

      {/* glass right*/}
      <mesh position={[6, 0, -3]}>
        <boxGeometry args={[5, 5, 5]} />
        <meshLambertMaterial color={COLOR_PALLETE.BROWN} />
      </mesh>

      {/* glass a */}
      <mesh>
        <boxGeometry args={[11, 1, 11]} />
        <meshLambertMaterial color={COLOR_PALLETE.BROWN} />
      </mesh>

      {/* ear left */}
      <mesh position={[0, 0, -6]}>
        <boxGeometry args={[2, 3, 2]} />
        <meshLambertMaterial color={COLOR_PALLETE.PINK} />
      </mesh>

      {/* ear left */}
      <mesh position={[0, 0, 6]}>
        <boxGeometry args={[2, 3, 2]} />
        <meshLambertMaterial color={COLOR_PALLETE.PINK} />
      </mesh>
    </mesh>
  );
};

export default React.memo(Pilot);
