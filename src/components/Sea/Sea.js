import { COLOR_PALLETE } from "../../utils/Colors";
import React, { useRef } from "react";
import { useFrame } from "@react-three/fiber";
import * as THREE from "three";
import useStore from "../../store/useStore";
import { expandDeltaTime } from "../../utils/UtilsFn";
const Sea = () => {
  const ref = useRef();
  const wave = [];
  const seaRadius = useStore((state) => state.seaRadius);
  const seaLength = useStore((state) => state.seaLength);
  const wavesMaxAmp = useStore((state) => state.wavesMaxAmp);
  const wavesMinAmp = useStore((state) => state.wavesMinAmp);
  const wavesMaxSpeed = useStore((state) => state.wavesMaxSpeed);
  const wavesMinSpeed = useStore((state) => state.wavesMinSpeed);
  const cylinderGeo = new THREE.CylinderGeometry(
    seaRadius,
    seaRadius,
    seaLength,
    40,
    10
  );
  cylinderGeo.applyMatrix4(new THREE.Matrix4().makeRotationX(-Math.PI / 2));

  const positionAttribute = cylinderGeo.attributes.position;
  const updatedPosition = [];
  cylinderGeo.morphAttributes.position = [];
  const waveMap = {};

  for (let i = 0; i < positionAttribute.count; i += 1) {
    const x = positionAttribute.getX(i);
    const y = positionAttribute.getY(i);
    const z = positionAttribute.getZ(i);
    updatedPosition.push(x, y, z);
    const index = wave.findIndex(
      (item) =>
        Math.abs(item.x - x) < 10 ** -10 &&
        Math.abs(item.y - y) < 10 ** -10 &&
        Math.abs(item.z - z) < 10 ** -10
    );
    if (index >= 0) {
      waveMap[i] = index;
    } else {
      waveMap[i] = wave.length;
      wave.push({
        x: x,
        y: y,
        z: z,
        // a random angle
        ang: Math.random() * Math.PI * 2,
        // a random distance
        amp: wavesMinAmp + Math.random() * (wavesMaxAmp - wavesMinAmp),
        // a random speed between 0.016 and 0.048 radians / frame
        speed: wavesMinSpeed + Math.random() * (wavesMaxSpeed - wavesMinSpeed),
      });
    }
  }
  cylinderGeo.morphAttributes.position[0] = new THREE.Float32BufferAttribute(
    updatedPosition,
    3
  );

  const moveWaves = (delta) => {
    const updatedPosition = [];

    for (let i = 0; i < positionAttribute.count; i += 1) {
      const index = waveMap[i];
      const vprops = wave[index];
      const x = vprops.x;
      const y = vprops.y;
      const z = vprops.z;
      updatedPosition.push(
        x + Math.cos(vprops.ang) * vprops.amp,
        y + Math.sin(vprops.ang) * vprops.amp,
        z
      );
    }

    for (let i = 0; i < wave.length; i++) {
      wave[i].ang += wave[i].speed * delta;
    }

    cylinderGeo.morphAttributes.position[0] = new THREE.Float32BufferAttribute(
      updatedPosition,
      3
    );
  };
  useFrame((_, delta) => {
    delta = expandDeltaTime(delta);
    ref.current.rotation.z += 0.005;
    if (ref.current.rotation.z > 2 * Math.PI) {
      ref.current.rotation.z -= 2 * Math.PI;
    }
    moveWaves(delta);
  });
  return (
    <mesh
      ref={ref}
      receiveShadow
      position={[0, -seaRadius, 0]}
      geometry={cylinderGeo}
      morphTargetInfluences={[1]}
    >
      <meshPhongMaterial
        color={COLOR_PALLETE.BLUE}
        transparent
        opacity={0.6}
        flatShading={true}
      />
    </mesh>
  );
};

export default React.memo(Sea);
