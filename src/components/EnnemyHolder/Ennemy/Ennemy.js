import { TetrahedronGeometry } from "three";
import { COLOR_PALLETE } from "../../../utils/Colors";
import useStore from "../../../store/useStore";
import { useFrame } from "@react-three/fiber";
import { useRef } from "react";
const Ennemy = ({ angle, dist }) => {
  //two data, angle, dist
  const seaRadius = useStore((state) => state.seaRadius);

  const ref = useRef(null);
  useFrame(() => {
    ref.current.rotation.z += Math.random() * 0.1;
    ref.current.rotation.y += Math.random() * 0.1;
  });

  return (
    <mesh
      ref={ref}
      castShadow
      position={[
        Math.cos(angle) * dist,
        -seaRadius + Math.sin(angle) * dist,
        0,
      ]}
    >
      <tetrahedronGeometry args={[8, 2]} />
      <meshPhongMaterial
        color={COLOR_PALLETE.RED}
        shininess={0}
        specular={0xffffff}
        flatShading
      />
    </mesh>
  );
};

export default Ennemy;
