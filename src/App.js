import "./App.css";
import Scene from "./components/Scene/Scene";
import { useState, useRef, useEffect } from "react";
import useStore from "./store/useStore";
import useKeypress from "react-use-keypress";
const App = () => {
  const energyBar = useRef(null);
  const handleReplay = useStore((state) => state.handleReplay);
  const distance = useStore((state) => state.distance);
  const level = useStore((state) => state.level);
  const distanceForLevelUpdate = useStore(
    (state) => state.distanceForLevelUpdate
  );
  const replayVisible = useStore((state) => state.replayVisible);

  const strokeDashOffset =
    502 * (1 - (distance % distanceForLevelUpdate) / distanceForLevelUpdate);

  useKeypress("Enter", () => handleReplay());
  return (
    <div className="game-holder" id="gameHolder">
      <div className="header">
        <h1>
          <span>the</span>Aviator
        </h1>
        <h2>fly it to the end</h2>
        <div className="score" id="score">
          <div className="score__content" id="level">
            <div className="score__label">level</div>
            <div className="score__value score__value--level" id="levelValue">
              {Math.floor(level)}
            </div>
            <svg
              className="level-circle"
              id="levelCircle"
              viewBox="0 0 200 200"
            >
              <circle
                id="levelCircleBgr"
                r="80"
                cx="100"
                cy="100"
                fill="none"
                stroke="#d1b790"
                strokeWidth="24px"
              ></circle>
              <circle
                id="levelCircleStroke"
                r="80"
                cx="100"
                cy="100"
                fill="none"
                color="#f25346"
                stroke="#68c3c0"
                strokeWidth="14px"
                strokeDasharray="502"
                strokeDashoffset={strokeDashOffset}
              ></circle>
            </svg>
          </div>
          <div className="score__content" id="dist">
            <div className="score__label">distance</div>
            <div className="score__value score__value--dist" id="distValue">
              {Math.floor(distance)}
            </div>
          </div>
          <div className="score__content" id="energy">
            <div className="score__label">energy</div>
            <div className="score__value score__value--energy" id="energyValue">
              <div className="energy-bar" ref={energyBar} id="energyBar"></div>
            </div>
          </div>
        </div>
      </div>
      <Scene />
      {replayVisible && (
        <div className="message message--replay" type="button">
          Press Enter to Replay
        </div>
      )}
      <div className="message message--instructions" id="instructions">
        <div>Grab the blue pills</div>
        <div>Avoid the red ones</div>
      </div>
      <div className="reference-container">
        Designed By Karim Maaloul. Coded By Stephen Fan.
      </div>
    </div>
  );
};

export default App;
