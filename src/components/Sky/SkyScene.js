import Cloud from "../Cloud/Cloud";
import { useRef, useEffect } from "react";
import { useFrame } from "@react-three/fiber";
import React from "react";
import useStore from "../../store/useStore";
import { expandDeltaTime } from "../../utils/UtilsFn";
const SkyScene = () => {
  const nClouds = 20;
  const stepAngle = (Math.PI * 2) / nClouds;
  const skyRef = useRef();
  const speedRef = useRef(useStore.getState().speed);
  const seaRadius = useStore((state) => state.seaRadius);
  useEffect(
    () =>
      useStore.subscribe(
        (speed) => (speedRef.current = speed),
        (state) => state.speed
      ),
    []
  );

  useFrame((_, delta) => {
    //maybe a problem
    delta = expandDeltaTime(delta);
    skyRef.current.rotation.z += speedRef.current * delta;
  });
  return (
    <group position={[0, -seaRadius, 0]} ref={skyRef}>
      {Array.from({ length: nClouds }).map((_, index) => {
        const a = stepAngle * index; // this is the final angle of the cloud
        const h = 750 + Math.random() * 200;
        const position = [
          Math.cos(a) * h,
          Math.sin(a) * h,
          -400 - Math.random() * 400,
        ];
        const rotation = [0, 0, a + Math.PI / 2];
        const scale = [
          1 + Math.random() * 2,
          1 + Math.random() * 2,
          1 + Math.random() * 2,
        ];
        return (
          <Cloud
            key={index}
            position={position}
            scale={scale}
            rotation={rotation}
          />
        );
      })}
    </group>
  );
};

export default React.memo(SkyScene);
