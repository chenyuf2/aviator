import { Canvas, useFrame } from "@react-three/fiber";
import Airplane from "../Airplane/Airplane";
import Sea from "../Sea/Sea";
import SkyScene from "../Sky/SkyScene";
import { useControls } from "leva";
import Lights from "../Lights/Lights";
import React from "react";
import { OrbitControls } from "@react-three/drei";
import CoinsHolder from "../CoinsHolder/CoinsHolder";
import { Html } from "@react-three/drei";
import useStore from "../../store/useStore";
const Scene = () => {
  // const data = useControls({
  //   metalness: { value: 0.0, min: 0, max: 1, step: 0.1 },
  //   roughness: { value: 0.5, min: 0, max: 1, step: 0.1 },
  //   directionIntensity: { value: 0.9, min: 0, max: 1, step: 0.1 },
  //   hemisphereIntensity: { value: 0.9, min: 0, max: 1, step: 0.1 },
  //   fogFar: { value: 950, min: 0, max: 1000, step: 10 },
  // });
  const replayVisible = useStore((state) => state.replayVisible);
  const handleReplay = useStore((state) => state.handleReplay);
  return (
    <>
      <Canvas
        shadows
        colorManagement
        flat={true}
        linear={true}
        gl={{
          alpha: true,
          antialias: true,
          shadowMap: true,
        }}
      >
        <Airplane />
        <SkyScene />
        <Sea />
        <CoinsHolder />
      </Canvas>
    </>
  );
};

export default Scene;
