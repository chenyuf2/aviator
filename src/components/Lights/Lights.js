import { useFrame, useThree } from "@react-three/fiber";
import React, { useRef } from "react";
import { PerspectiveCamera } from "@react-three/drei";
import { expandDeltaTime, normalizeFn } from "../../utils/UtilsFn";
import useStore from "../../store/useStore";

const PLAYING = "playing";
const Lights = ({ planeMesh }) => {
  const cameraRef = useRef(null);
  const { mouse } = useThree();
  const ambientRef = useRef(null);
  const cameraSensivity = useStore((state) => state.cameraSensivity);
  const planeDefaultHeight = useStore((state) => state.planeDefaultHeight);
  const status = useStore((state) => state.status);
  const ambientLightIntensity = useStore(
    (state) => state.ambientLightIntensity
  );
  useFrame((_, delta) => {
    delta = expandDeltaTime(delta);
    if (status === PLAYING) {
      cameraRef.current.fov = normalizeFn(mouse.x, -1, 1, 40, 80);
      cameraRef.current.updateProjectionMatrix();
      cameraRef.current.position.y +=
        (planeMesh.position.y - cameraRef.current.position.y) *
        delta *
        cameraSensivity;
    }
    useStore.setState({
      ambientLightIntensity:
        ambientLightIntensity + (0.5 - ambientLightIntensity) * delta * 0.005,
    });
    // ambientRef.current.intensity +=
    //   (0.5 - ambientRef.current.intensity) * delta * 0.005;
  });
  return (
    <>
      <PerspectiveCamera
        ref={cameraRef}
        makeDefault
        position={[0, planeDefaultHeight, 200]}
        far={10000}
        near={0.1}
        fov={50}
      />
      <fog attach="fog" args={[0xf7d9aa, 100, 950]} />
      <hemisphereLight args={[0xaaaaaa, 0x000000, 0.9]} />
      <ambientLight
        args={[0xdc8874, 0.5]}
        ref={ambientRef}
        intensity={ambientLightIntensity}
      />
      <directionalLight
        castShadow
        args={[0xffffff, 0.9]}
        shadow-mapSize-height={4096}
        shadow-mapSize-width={4096}
        shadow-camera-bottom={-400}
        shadow-camera-left={-400}
        shadow-camera-top={400}
        shadow-camera-right={400}
        shadow-camera-near={1}
        shadow-camera-far={1000}
        position={[150, 350, 350]}
      />
    </>
  );
};

export default Lights;
