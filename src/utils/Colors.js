export const COLOR_PALLETE = {
  RED: "#f25346",
  WHITE: "#d8d0d1",
  BROWN: "#59332e",
  PINK: "#f5986e",
  BROWNDARK: "#23190f",
  BLUE: "#68c3c0",
};
