import useStore from "../../store/useStore";
import Ennemy from "./Ennemy/Ennemy";
const EnnemyHolder = () => {
  const ennemiesInUse = useStore((state) => state.ennemiesInUse);
  return (
    <>
      {ennemiesInUse?.map((item, index) => (
        <Ennemy key={index} angle={item.angle} dist={item.dist} />
      ))}
    </>
  );
};
export default EnnemyHolder;
