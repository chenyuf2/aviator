import { useFrame } from "@react-three/fiber";
import { useRef } from "react";
import useStore from "../../../store/useStore";
const Coin = ({ angle, dist, exploding = false, rotation }) => {
  //possible two dynamic data, dist, angle
  const seaRadius = useStore((state) => state.seaRadius);
  const ref = useRef();

  useFrame(() => {
    ref.current.rotation.z += Math.random() * 0.1;
    ref.current.rotation.y += Math.random() * 0.1;
  });
  return (
    <mesh
      castShadow
      ref={ref}
      position={[
        Math.cos(angle) * dist,
        -seaRadius + Math.sin(angle) * dist,
        0,
      ]}
      rotation={rotation}
    >
      <tetrahedronGeometry args={[5, 0]} />
      <meshPhongMaterial
        color={0x009999}
        shininess={0}
        specular={0xffffff}
        flatShading
      />
    </mesh>
  );
};

export default Coin;
