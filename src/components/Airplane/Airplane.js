import { COLOR_PALLETE } from "../../utils/Colors";
import { useFrame } from "@react-three/fiber";
import React, { useEffect, useMemo, useRef, useState } from "react";
import { useThree } from "@react-three/fiber";
import { normalizeFn } from "../../utils/UtilsFn";
import * as THREE from "three";
import Pilot from "../Pilot/Pilot";
import Lights from "../Lights/Lights";
import CoinsHolder from "../CoinsHolder/CoinsHolder";
import EnnemyHolder from "../EnnemyHolder/EnnemyHolder";
import produce from "immer";
import useStore from "../../store/useStore";
import { expandDeltaTime } from "../../utils/UtilsFn";

const PLAYING = "playing";
const GAMEOVER = "gameover";
const WAITINGREPLAY = "waitingReplay";

const Airplane = () => {
  const propellerRef = useRef();
  const planeRef = useRef();
  const cockpitRef = useRef(null);
  const { mouse } = useThree();

  const coinsInUse = useStore((state) => state.coinsInUse);
  const coinsPool = useStore((state) => state.coinsPool);
  const status = useStore((state) => state.status);
  const planeSpeed = useStore((state) => state.planeSpeed);
  const updatePlane = useStore((state) => state.updatePlane);
  const planeDefaultHeight = useStore((state) => state.planeDefaultHeight);
  const updateDistance = useStore((state) => state.updateDistance);
  const updateEnergy = useStore((state) => state.updateEnergy);
  const distance = useStore((state) => state.distance);
  const distanceForSpeedUpdate = useStore(
    (state) => state.distanceForSpeedUpdate
  );
  const speedLastUpdate = useStore((state) => state.speedLastUpdate);
  const levelLastUpdate = useStore((state) => state.levelLastUpdate);
  const distanceForLevelUpdate = useStore(
    (state) => state.distanceForLevelUpdate
  );
  const targetBaseSpeed = useStore((state) => state.targetBaseSpeed);
  const incrementSpeedByTime = useStore((state) => state.incrementSpeedByTime);
  const level = useStore((state) => state.level);
  const initSpeed = useStore((state) => state.initSpeed);
  const incrementSpeedByLevel = useStore(
    (state) => state.incrementSpeedByLevel
  );
  const baseSpeed = useStore((state) => state.baseSpeed);
  const speed = useStore((state) => state.speed);
  const planeFallSpeed = useStore((state) => state.planeFallSpeed);
  const setReplayVisible = useStore((state) => state.setReplayVisible);
  const seaRadius = useStore((state) => state.seaRadius);

  const planeAmpHeight = useStore((state) => state.planeAmpHeight);
  const coinLastSpawn = useStore((state) => state.coinLastSpawn);
  const distanceForCoinsSpawn = useStore(
    (state) => state.distanceForCoinsSpawn
  );
  const rotateCoins = useStore((state) => state.rotateCoins);
  const spawnEnnemies = useStore((state) => state.spawnEnnemies);
  const rotateEnnemies = useStore((state) => state.rotateEnnemies);

  const coinDistanceTolerance = useStore(
    (state) => state.coinDistanceTolerance
  );
  const spawnCoins = useStore((state) => state.spawnCoins);

  const coinsSpeed = useStore((state) => state.coinsSpeed);
  const addEnergy = useStore((state) => state.addEnergy);
  const distanceForEnnemiesSpawn = useStore(
    (state) => state.distanceForEnnemiesSpawn
  );
  const ennemyLastSpawn = useStore((state) => state.ennemyLastSpawn);

  const particlesHolderRef = useRef(null);

  const suspenseGeometry = useMemo(() => {
    const suspenseGeometry = new THREE.BoxGeometry(4, 20, 4);
    suspenseGeometry.applyMatrix4(
      new THREE.Matrix4().makeTranslation(0, 10, 0)
    );
    return suspenseGeometry;
  });
  useEffect(() => {
    const geometry = cockpitRef.current.geometry;
    const positionAttribute = geometry.attributes.position;
    const vertices = positionAttribute.array;
    const updatedPosition = [];
    geometry.morphAttributes.position = [];

    for (let i = 0; i < positionAttribute.count; i++) {
      const x = positionAttribute.getX(i);
      const y = positionAttribute.getY(i);
      const z = positionAttribute.getZ(i);
      if (x === -40 && y === 25 && z === -25) {
        updatedPosition.push(x, y - 10, z + 20);
      } else if (x === -40 && y === 25 && z === 25) {
        updatedPosition.push(x, y - 10, z - 20);
      } else if (x === -40 && y === -25 && z === -25) {
        updatedPosition.push(x, y + 30, z + 20);
      } else if (x === -40 && y === -25 && z === 25) {
        updatedPosition.push(x, y + 30, z - 20);
      } else {
        updatedPosition.push(x, y, z);
      }
    }

    geometry.morphAttributes.position[0] = new THREE.Float32BufferAttribute(
      updatedPosition,
      3
    );
  });

  useFrame((_, delta) => {
    //
    delta = expandDeltaTime(delta);
    if (status === PLAYING) {
      if (
        Math.floor(distance) % distanceForCoinsSpawn == 0 &&
        Math.floor(distance) > coinLastSpawn
      ) {
        useStore.setState({
          coinLastSpawn: Math.floor(distance),
        });
        spawnCoins();
      }

      if (
        Math.floor(distance) % distanceForSpeedUpdate == 0 &&
        Math.floor(distance) > speedLastUpdate
      ) {
        useStore.setState({
          speedLastUpdate: Math.floor(distance),
          targetBaseSpeed: targetBaseSpeed + incrementSpeedByTime * delta,
        });
      }

      if (
        Math.floor(distance) % distanceForEnnemiesSpawn == 0 &&
        Math.floor(distance) > ennemyLastSpawn
      ) {
        useStore.setState({
          ennemyLastSpawn: Math.floor(distance),
        });
        spawnEnnemies();
      }

      if (
        Math.floor(distance) % distanceForLevelUpdate == 0 &&
        Math.floor(distance) > levelLastUpdate
      ) {
        useStore.setState({
          levelLastUpdate: Math.floor(distance),
          level: level + 1,
          targetBaseSpeed: initSpeed + incrementSpeedByLevel * level,
        });
      }

      updatePlane(delta, mouse, planeRef);
      updateDistance(delta);
      updateEnergy(delta);

      useStore.setState({
        baseSpeed: baseSpeed + (targetBaseSpeed - baseSpeed) * delta * 0.02,
        speed: baseSpeed * planeSpeed,
      });
    } else if (status === GAMEOVER) {
      useStore.setState({
        speed: speed * 0.99,
        planeFallSpeed: planeFallSpeed * 1.05,
      });

      planeRef.current.rotation.z +=
        (-Math.PI / 2 - planeRef.current.rotation.z) * 0.0002 * delta;
      planeRef.current.rotation.x += 0.0003 * delta;
      planeRef.current.position.y -= planeFallSpeed * delta;

      if (planeRef.current.position.y < -200) {
        setReplayVisible();
        useStore.setState({
          status: "waitingReplay",
        });
      }
    }

    rotateCoins(delta, planeRef, particlesHolderRef.current);
    rotateEnnemies(delta, planeRef, particlesHolderRef.current);

    propellerRef.current.rotation.x += 0.2 + planeSpeed * delta * 0.005;
  });

  return (
    <>
      <Lights planeMesh={planeRef.current} />
      <group
        position={[0, planeDefaultHeight, 0]}
        scale={[0.25, 0.25, 0.25]}
        ref={planeRef}
      >
        <mesh castShadow={true} receiveShadow position={[50, 0, 0]}>
          <boxGeometry args={[20, 50, 50, 1, 1, 1]} />
          <meshPhongMaterial
            attach="material"
            color={COLOR_PALLETE.WHITE}
            flatShading={true}
            metalness={0}
            roughness={0.5}
          />
        </mesh>

        <mesh castShadow={true} receiveShadow position={[-40, 20, 0]}>
          <boxGeometry args={[15, 20, 5, 1, 1, 1]} />
          <meshPhongMaterial
            attach="material"
            color={COLOR_PALLETE.RED}
            flatShading={true}
            metalness={0}
            roughness={0.5}
          />
        </mesh>

        <mesh castShadow={true} receiveShadow position={[0, 15, 0]}>
          <boxGeometry attach="geometry" args={[30, 5, 120, 1, 1, 1]} />
          <meshPhongMaterial
            attach="material"
            color={COLOR_PALLETE.RED}
            flatShading={true}
            metalness={0}
            roughness={0.5}
          />
        </mesh>

        <mesh castShadow={true} receiveShadow position={[5, 27, 0]}>
          <boxGeometry attach="geometry" args={[3, 15, 20, 1, 1, 1]} />
          <meshPhongMaterial
            attach="material"
            color={COLOR_PALLETE.WHITE}
            flatShading={true}
            opacity={0.3}
            transparent
            metalness={0}
            roughness={0.5}
          />
        </mesh>

        {/* geomPropeller */}

        <mesh
          castShadow={true}
          receiveShadow
          position={[60, 0, 0]}
          ref={propellerRef}
        >
          <boxGeometry attach="geometry" args={[20, 10, 10, 1, 1, 1]} />
          <meshPhongMaterial
            attach="material"
            color={COLOR_PALLETE.BROWNDARK}
            flatShading={true}
            metalness={0}
            roughness={0.5}
          />

          <mesh castShadow={true} receiveShadow position={[8, 0, 0]}>
            <boxGeometry args={[1, 80, 10, 1, 1, 1]} />
            <meshPhongMaterial
              attach="material"
              color={COLOR_PALLETE.BROWNDARK}
              flatShading={true}
              metalness={0}
              roughness={0.5}
            />
          </mesh>

          <mesh
            castShadow={true}
            rotation={[Math.PI / 2, 0, 0]}
            receiveShadow
            position={[8, 0, 0]}
          >
            <boxGeometry args={[1, 80, 10, 1, 1, 1]} />
            <meshPhongMaterial
              attach="material"
              color={COLOR_PALLETE.BROWNDARK}
              flatShading={true}
              metalness={0}
              roughness={0.5}
            />
          </mesh>
        </mesh>

        {/* wheelProtecR */}

        <mesh castShadow={true} receiveShadow position={[25, -20, 25]}>
          <boxGeometry args={[30, 15, 10, 1, 1, 1]} />
          <meshPhongMaterial
            attach="material"
            color={COLOR_PALLETE.RED}
            flatShading={true}
            metalness={0}
            roughness={0.5}
          />
        </mesh>

        {/* wheelTireR */}
        <mesh castShadow={true} receiveShadow position={[25, -28, 25]}>
          <boxGeometry args={[24, 24, 4]} />
          <meshPhongMaterial
            attach="material"
            color={COLOR_PALLETE.BROWNDARK}
            flatShading={true}
            metalness={0}
            roughness={0.5}
          />
          <mesh>
            <boxGeometry args={[10, 10, 6]} />
            <meshPhongMaterial
              attach="material"
              color={COLOR_PALLETE.BROWN}
              flatShading={true}
              metalness={0}
              roughness={0.5}
            />
          </mesh>
        </mesh>

        <mesh castShadow={true} receiveShadow position={[25, -20, -25]}>
          <boxGeometry args={[30, 15, 10, 1, 1, 1]} />
          <meshPhongMaterial
            attach="material"
            color={COLOR_PALLETE.RED}
            flatShading={true}
            metalness={0}
            roughness={0.5}
          />
        </mesh>

        {/* wheelTireL */}
        <mesh castShadow={true} receiveShadow position={[25, -28, -25]}>
          <boxGeometry args={[24, 24, 4]} />
          <meshPhongMaterial
            attach="material"
            color={COLOR_PALLETE.BROWNDARK}
            flatShading={true}
            metalness={0}
            roughness={0.5}
          />
          <mesh>
            <boxGeometry args={[10, 10, 6]} />
            <meshPhongMaterial
              attach="material"
              color={COLOR_PALLETE.BROWN}
              flatShading={true}
              metalness={0}
              roughness={0.5}
            />
          </mesh>
        </mesh>

        {/* wheelBack */}
        <mesh
          castShadow={true}
          receiveShadow
          position={[-35, -5, 0]}
          scale={[0.5, 0.5, 0.5]}
        >
          <boxGeometry args={[24, 24, 4]} />
          <meshPhongMaterial
            attach="material"
            color={COLOR_PALLETE.BROWNDARK}
            flatShading={true}
            metalness={0}
            roughness={0.5}
          />
          <mesh>
            <boxGeometry args={[10, 10, 6]} />
            <meshPhongMaterial
              attach="material"
              color={COLOR_PALLETE.BROWN}
              flatShading={true}
              metalness={0}
              roughness={0.5}
            />
          </mesh>
        </mesh>

        {/* suspensense */}
        <mesh
          position={[-35, -5, 0]}
          rotation={[0, 0, -0.3]}
          geometry={suspenseGeometry}
        >
          <meshPhongMaterial color={COLOR_PALLETE.RED} flatShading />
        </mesh>

        {/* cockpit */}
        <mesh
          ref={cockpitRef}
          morphTargetInfluences={[1]}
          castShadow
          receiveShadow
        >
          <boxGeometry args={[80, 50, 50, 1, 1, 1]} />
          <meshPhongMaterial color={COLOR_PALLETE.RED} flatShading />
        </mesh>

        {/* Pilot */}
        <Pilot />
      </group>
      <CoinsHolder />
      <EnnemyHolder />
      <object3D ref={particlesHolderRef}></object3D>
    </>
  );
};
export default Airplane;
